# Js Cart

Simple Javascript Cart manager

### Installation

```bash
npm install kisphp-cart-manager --save
```

### Usage

```javascript

var cart = Cart;

var product_1 = {
    id: 1,
    title: "Product 1",
    description: "Product description"
    // .... more product related data ....
};

```

> `id` property is mandatory
>
> `quantity` will be added automatically

### Add product to cart

```javascript
// quantity is an integer
cart.add(product_1, quantity);
```

### subtract quantity from cart
```javascript
// if quantity > available quantity in cart, then it will delete the product from cart
cart.subtract(product_1, quantity);
```

### remove product from cart
```javascript
cart.remove(product);
```

See [Example](./example.html) for a demo
