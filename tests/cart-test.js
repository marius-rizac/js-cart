'use strict';

let cart = require('../src/cart');

let chai = require('chai');
let expect = chai.expect; // we are using the "expect" style of Chai

describe('Cart', function() {

    it('should add one product with quantity 2', () => {
        let p1 = {
            id: 1,
            title: 'Product 1'
        };
        cart.add(p1, '2');

        expect(cart.products.length).to.equal(1, 'It must be only one product in cart');
        expect(cart.products[0].quantity).to.equal(2, 'Quantity for product 1 must be 2');
    });

    it('should add second product with quantity 3', () => {
        let p2 = {
            id: 2,
            title: 'Product 2'
        };
        cart.add(p2, '3');

        expect(cart.products.length).to.equal(2, 'It must be 2 products in cart');
        expect(cart.products[0].quantity).to.equal(2, 'Quantity for product 1 must be 2');
        expect(cart.products[1].quantity).to.equal(3, 'Quantity for product 2 must be 3');
    });

    it('should increase quantity when adding the same product to cart', () => {
        let p1 = {
            id: 1,
            title: 'Product 1'
        };
        cart.add(p1, '3');

        expect(cart.products.length).to.equal(2, 'It must be 2 products in cart');
        expect(cart.products[0].quantity).to.equal(5, 'Quantity for product 1 must be 5');
    });

    it('should decrease quantity for product 1 by 1', () => {
        let p1 = {
            id: 1,
            title: 'Product 1'
        };
        cart.subtract(p1, '1');

        expect(cart.products.length).to.equal(2, 'It must be 2 products in cart');
        expect(cart.products[0].quantity).to.equal(4, 'Quantity for product 1 must be 4');
    });

    it('should delete the product from cart', () => {
        let p2 = {
            id: 2,
            title: 'Product 2'
        };
        cart.remove(p2);

        expect(cart.products.length).to.equal(1, 'It must be 1 product in cart');
        expect(cart.products[0].quantity).to.equal(4, 'Quantity for product 1 must be 4');
    });

    it('should remove product from cart if subtract more than quantity', () => {
        let p1 = {
            id: 1,
            title: 'Product 1'
        };
        cart.subtract(p1, '10');

        expect(cart.products.length).to.equal(0, 'The cart should be empty');
    });

    it('it should add 3 product and remove only the second', () => {
        let p1 = { id: 1, title: 'Product 1' };
        let p2 = { id: 2, title: 'Product 2' };
        let p3 = { id: 3, title: 'Product 3' };

        cart.add(p1, 1);
        cart.add(p2, 1);
        cart.add(p3, 1);

        cart.remove(p2);

        expect(cart.products.length).to.equal(2, 'It must be 1 product in cart');
    });
});
