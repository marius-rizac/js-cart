let gulp = require('gulp');
let browserify = require('gulp-browserify');
let concat = require('gulp-concat');

let config = {
    sources: [
        'src/cart.js', // main cart class
        'src/browser.js', // browserify compiler for browser
    ],
    target: {
        filename: 'cart.js',
        dirname: 'dist/'
    }
};

gulp.task('default', () => {
    return gulp.src(config.sources)
    .pipe(concat(config.target.filename))
    .pipe(browserify({
        insertGlobals: true,
        debug: false
    }))
    .pipe(gulp.dest(config.target.dirname));
});
