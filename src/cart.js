class Cart {

  constructor() {
    this.products = [];
  }

  add(product, qty) {
    let self = this;
    let product_already_in_cart = false;
    let quantity = parseInt(qty);

    self.products.forEach((item) => {
      if (item.id === product.id) {
        item.quantity += quantity;
        product_already_in_cart = true;
      }
    });

    if (product_already_in_cart) {
      return true;
    }

    product.quantity = quantity;
    self.products.push(product);
  }

  subtract(product, qty) {
    let self = this;
    let quantity = parseInt(qty);

    self.products.forEach((item) => {
      if (product.id === item.id) {
        item.quantity -= quantity;

        if (item.quantity < 1) {
          self.remove(product);
        }
      }
    });
  }

  remove(product) {
    let self = this;

    self.products.forEach((item, index) => {
      if (product.id === item.id) {
        self.products.splice(index, 1);
      }
    });
  }
}

module.exports = new Cart();
